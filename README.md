# Programa de teste para LCD

## Nova biblioteca
Para poder utilizar o LCD I2C no ESP32 tem que ser baixada uma nova biblioteca, para baixa-la clique [aqui](https://github.com/marcoschwartz/LiquidCrystal_I2C/archive/master.zip)
Tutorial de instalacao veja [aqui.](https://randomnerdtutorials.com/esp32-esp8266-i2c-lcd-arduino-ide/)
