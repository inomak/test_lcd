#include <LiquidCrystal_I2C.h>

#define ADDR 0x27
#define COL 16
#define ROW 2

LiquidCrystal_I2C lcd(ADDR, COL, ROW);


void setup() {
  lcd.init();
  lcd.backlight();
}


void loop() {

  lcd.setCursor(0, 0);
  lcd.print("Hello world!");
  lcd.setCursor(0, 1);
  lcd.print("Ola mundo!");

  delay(1000);
  lcd.clear();

  delay(300);
}